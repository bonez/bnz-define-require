do ->

    modules = {}
    cache = {}

    window.require = (name) ->
        if cache.hasOwnProperty name then return cache[name].exports
        if modules.hasOwnProperty name
            module = id: name, exports: {}
            switch typeof modules[name]
                when 'object' then module.exports = modules[name]
                when 'function' then modules[name] module
            modules[name] = module.exports
            cache[name] = module
            return module.exports
        # throw new Error "Module \"#{name}\" doesn't not exists!"
        false

    window.define = (name, fn) ->
        modules[name] = fn
        return

    # UpperCase First
    upc = (str) -> "#{str.charAt(0).toUpperCase()}#{str.slice 1}"

    window.bnz = (type) ->
        if not type? then return modules
        if type is '' then return {}
        res = {}
        for name, module of modules when (new RegExp "#{upc type}$").test name then res[name] = module
        res


    return
